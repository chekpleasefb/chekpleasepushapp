import { LoginPage } from './../login/login';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

 
  name:string ="";
	@ViewChild('username') user;
	@ViewChild('password') password;

  constructor(private alertCtrl: AlertController, private fire: AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, private db:AngularFireDatabase) {
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');

return this.db.database.ref('/users/zRmfnvSJtweTQDWb4JrL3dwhLQS2').once('value').then(function(snapshot) { 
  console.log("snapshot value "+ snapshot.val().firstName);
})
    
  }

  alert(message: string) {
    this.alertCtrl.create({
      title: 'Info!',
      subTitle: message,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.push(LoginPage);
          }
        }]
    }).present();
  }

  registerUser() {
    this.fire.auth.createUserWithEmailAndPassword(this.user.value, this.password.value)
    .then(data => {
      console.log('got data ', data);
     
      this.fire.auth.currentUser.updateProfile(
        {
          displayName: this.name,
          photoURL: ''
        }
      ).then(()=>{
        console.log("username updated");
       
        
        console.log("uid "+ this.fire.auth.currentUser.uid);

      }).then(()=>{
        this.db.database.ref('users/' + this.fire.auth.currentUser.uid).set({
          firstName: this.fire.auth.currentUser.displayName,
          email: this.fire.auth.currentUser.email
        });
        this.alert('Registered!');
      });
    })
    .catch(error => {
      console.log('got an error ', error);
      this.alert(error.message);
    });
  	console.log('Would register user with ', this.user.value, this.password.value);
  }

}