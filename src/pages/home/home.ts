import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { PushOptions, PushObject, Push } from '@ionic-native/push';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  senderid;
  constructor(public navCtrl: NavController, private push: Push, public alertCtrl: AlertController, private db:AngularFireDatabase,private fire: AngularFireAuth) {
    this.pushnotification()
  }

signout(){
 this.navCtrl.push(LoginPage);
}
test1(){
  this.db.database.ref('newOrders/').push({
    sender:this.senderid,
     receiver:'LxGI21oLeSayxoCyN7YOiWzLRBs1',
    item:'pepsi',
    table:'7'
  });
}
test2(){
  this.db.database.ref('newOrders/').push({
    sender:this.senderid,
    receiver:'e3iXNZxM2VhWettKTXlaHot3kM72',
    item:'Coca Cola',
    table:'5'
  });
}
pushnotification(){
  const options: PushOptions = {
    android: {},
    ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
    },
 };
 
 const pushObject: PushObject = this.push.init(options);
 
 pushObject.on('notification').subscribe((notification: any) => this.alert(notification.message));
 
 pushObject.on('registration').subscribe((registration: any) => 
 {  this.senderid=registration.registrationId
   let uid=this.fire.auth.currentUser.uid
  
   this.db.database.ref('tokens/' + uid).set(this.senderid);
  //  this.db.database.ref('tokens/'+uid).once('value').then((data)=>{
  //   console.log("data value "+ data.val());
  //  })

});
 
 pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
}
alert(message: string) {
  this.alertCtrl.create({
    title: 'Notification!',
    subTitle: message,
    buttons: ['OK']
  }).present();
}
}
